<?php

namespace app\models;
class Day extends \yii\base\Model
{
    public $day;
    public $isWeekend = false;

    public function checkDay($day)
    {
        switch ($day) {
            case 'Saturday':
            case 'Sunday':
                $this->isWeekend = true;
                break;
            default:
                $this->isWeekend = false;
        }
    }
}